use itertools;
use itertools::Itertools;
use std::str::FromStr;
use text_io;
use std::error::Error;
use std::io::Read;
use std::collections::HashSet;
use std::convert::TryInto;
use std::ops::Neg;

#[aoc(day9, part1, Dirty)]
pub fn not_adding_up(input: &str) -> usize {
    let input: Vec<usize> = input.lines().map(|i| i.parse().unwrap()).collect();
    for i in input.windows(26) {
        if !i[0..25].iter().tuple_combinations().find(|(&a, &b)| a+b == i[25]).is_some() {
            return i[25];
        }
    }
    panic!()
}

#[aoc(day9, part2, Dirty)]
pub fn weakness_set(input: &str) -> usize {
    let input: Vec<usize> = input.lines().map(|i| i.parse().unwrap()).collect();
    let invalid = 22406676;
    for i in 0..input.len() {
        for j in i+1..=input.len() {
            if input[i..j].iter().sum::<usize>() == invalid {;return input[i..j].iter().min().unwrap() + input[i..j].iter().max().unwrap();}
        }
    }
    panic!()
}
