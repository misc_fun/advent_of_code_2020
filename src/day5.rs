use itertools;
use itertools::Itertools;
use std::str::FromStr;
use text_io;
use std::error::Error;
use std::io::Read;

#[aoc(day5, part1, Dirty)]
fn boarding(input: &str) -> i64 {
    let mut max = 0;
    for l in input.lines() {
        let mut h = 64;
        let mut hs = 0;
        let mut he = 127;
        for c in l.bytes().take(7) {
            let c = c as char;
            match c {
                'F' => he -= h,
                'B' => hs += h,
                _ => panic!("Char not understood")
            }
            h /= 2;
        }
        let mut w = 4;
        let mut ws = 0;
        let mut we = 7;
        for c in l.bytes().skip(7).take(3) {
            let c = c as char;

            match c {
                'L' => we -= w,
                'R' => ws += w,
                _ => panic!("Char not understood")
            }
            w /= 2;
        }
        let v = hs * 8 + ws;
        if v > max {max = v;}
    }
    max
}


pub fn ids(input: &str) -> Vec<i64> {
    let mut ids = Vec::new();
    for l in input.lines() {
        let mut h = 64;
        let mut hs = 0;
        let mut he = 127;
        for c in l.bytes().take(7) {
            let c = c as char;
            match c {
                'F' => he -= h,
                'B' => hs += h,
                _ => panic!("Char not understood")
            }
            h /= 2;
        }
        let mut w = 4;
        let mut ws = 0;
        let mut we = 7;
        for c in l.bytes().skip(7).take(3) {
            let c = c as char;

            match c {
                'L' => we -= w,
                'R' => ws += w,
                _ => panic!("Char not understood")
            }
            w /= 2;
        }
        let v = hs * 8 + ws;
        ids.push(v);
    }
    ids
}



#[aoc(day5, part2, Dirty)]
fn boarding_id(input: &str) -> i64 {
    let mut ids = ids(input);
    ids.sort();
    println!("{:?}", ids);
    for i in ids.windows(2) {
        if i[0] + 2 == i[1] {return i[0] + 1}
    }
    panic!()
}