use itertools;
use itertools::Itertools;
use std::str::FromStr;
use text_io;
use std::error::Error;
use std::io::Read;
use std::collections::HashSet;
use std::convert::TryInto;
use std::ops::Neg;

#[aoc(day10, part1, Dirty)]
pub fn jolts(input: &str) -> usize {/*
    let input = "28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3
";
    let input = "16
10
15
5
1
11
7
19
6
12
4
";*/
    //how to use map instead
    let mut diffs: [usize; 4] = [0; 4];
    //how to append 0
    input.lines().map(|l| {l.parse::<usize>().unwrap()}).sorted().into_iter().tuple_windows().for_each(|(a, b)| diffs[b-a] += 1);
    dbg!((diffs[1]+1)) * dbg!((diffs[3]+1))
}

#[aoc(day10, part2, Dirty)]
pub fn jolt_arrangements(input: &str) -> usize {/*
    let input = "16
10
14
5
1
3
11
7
19
6
12
4";*//*
    let input = "28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3
";*/

    let mut prev = vec![0, 0, 1, 1];// 3 previous values + current value Buffer. Don't take into account the padded inputs, except for one (hence the first 1) representing the starting 0 value
    let mut data = vec![0, 0, 0];// padded input

    data.append(&mut input.lines().map(|l| {l.parse::<usize>().unwrap()}).collect());

    data.iter().sorted().into_iter().tuple_windows().for_each(|(&a, &b, &c, &d)| {
        // a can jump to the current value (d)
        if d - a <= 3 { prev[3] += prev[0]; };
        // b can jump to the current value (d)
        if d - b <= 3 { prev[3] += prev[1]; };

        prev.rotate_left(1);// wheeee!
        *prev.last_mut().unwrap() = prev[2];// prepare for the next turn: you can alway "jump" from the previous value to the current one

    });
    prev[3]
}
