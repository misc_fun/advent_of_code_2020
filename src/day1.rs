use itertools;
use itertools::Itertools;
use std::cmp::Ordering;

#[aoc_generator(day1)]
fn gen(input: &str) -> Vec<usize> {
    input.lines().filter_map(|s| Some(s.trim().parse().unwrap())).collect()
}



#[aoc(day1, part1, Dirty)]//incorrect, but sufficient here
fn twenty_twenty(numbers: &[usize]) -> usize {
    for n in numbers.iter() {
        for p in numbers.iter() {
            if n + p == 2020 {
                return n * p;
            }
        }
    }
    panic!("Found nothing!");
}


#[aoc(day1, part1, Optimized)]
fn twenty_twenty_optimized(numbers: &[usize]) -> usize {
    // not that fast, since it needs sorting
    let mut numbers = numbers.to_vec(); numbers.sort();
    let mut start = 0;
    let mut stop = numbers.len() - 1;
    while start < stop {
        match (numbers[start] + numbers[stop]).cmp(&2020) {
            Ordering::Equal => return numbers[start] * numbers[stop],
            Ordering::Greater => stop -= 1,
            Ordering::Less => start += 1
        }
    }
    panic!("Found nothing!");
}

#[aoc(day1, part2, Dirty)]//incorrect, but sufficient here
fn twenty_twenty_twenty(numbers: &[usize]) -> usize {
    for n in numbers.iter() {
        for p in numbers.iter() {
            for q in numbers.iter() {
                if n + p + q == 2020 {
                    return n * p * q;
                }
            }
        }
    }
    panic!("Found nothing!");
}

#[aoc(day1, part2, Fast)]
fn twenty_twenty_twenty_fast(mut numbers: &[usize]) -> usize {
    let mut numbers = numbers.to_vec(); numbers.sort();
    let min = numbers[0];

    for (i, n) in numbers.iter().enumerate() {
        if *n + min > 2020 {break;}
        for (j, p) in numbers.iter().skip(i+1).enumerate() {
            if *n + *p + min > 2020 {break;}
            for q in numbers.iter().skip(j+1) {
                    if n + p + q == 2020 {
                        return n * p * q;
                    }
                if *n + *p + *q > 2020 {break;}
            }
        }
    }
    panic!("Found nothing!");
}

#[aoc(day1, part2, Optimized)]
fn twenty_twenty_twenty_optimized(mut numbers: &[usize]) -> usize {
    // not that fast, since it needs sorting
    let mut numbers = numbers.to_vec(); numbers.sort();
    let last = numbers.len() - 1;
    for (i, n) in numbers.iter().take(last-2).enumerate() {
        let mut start = i+1;
        let mut stop = last;
        while start < stop {
            match (n + numbers[start] + numbers[stop]).cmp(&2020) {
                Ordering::Equal => return n * numbers[start] * numbers[stop],
                Ordering::Greater => stop -= 1,
                Ordering::Less => start += 1
            }
        }
    }
    panic!("Found nothing!");
}

#[aoc(day1, part2, Short)]
fn twenty_twenty_twenty_short(numbers: &[usize]) -> usize {
    numbers.iter().tuple_combinations().find(|(&a, &b, &c)| a+b+c == 2020).and_then(|(&a, &b, &c)| Some(a*b*c)).unwrap()
}
