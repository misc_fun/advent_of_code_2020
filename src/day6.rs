use itertools;
use itertools::Itertools;
use std::str::FromStr;
use text_io;
use std::error::Error;
use std::io::Read;
use std::collections::HashSet;


#[aoc(day6, part1, Dirty)]
pub fn sum_union(input: &str) -> usize {
    input.split("\n\n").map(|g| {
        let mut chars = HashSet::new();
        let mut it = g.lines();
        for c in it.next().unwrap().bytes() {
            let c = c as char;
            if c.is_alphabetic() {
                chars.insert(c);
            }
        }
        for l in it {
            let mut c2 = HashSet::new();
            for c in l.bytes() {
                let c = c as char;
                if c.is_alphabetic() {
                    c2.insert(c);
                }
            }
            // wanted to use `union`, but ended up doing an ugly dance around ownership. Couldn't find a way to mutate in place
            let union = chars.union(&c2).collect::<HashSet<_>>();
            //( ew
            let mut t = HashSet::new();
            for &&i in union.iter() {
                t.insert(i);
            }
            chars.clear();
            for &i in t.iter() {
                chars.insert(i);
            }
            //) ew
        }
        chars.len()
    }).sum()
}

#[aoc(day6, part2, Dirty)]
pub fn sum_intersection(input: &str) -> usize {
    input.split("\n\n").map(|g| {
        let mut chars = HashSet::new();
        let mut it = g.lines();
        for c in it.next().unwrap().bytes() {
            let c = c as char;
            if c.is_alphabetic() {
                chars.insert(c);
            }
        }
        for l in it {
            let mut c2 = HashSet::new();
            for c in l.bytes() {
                let c = c as char;
                if c.is_alphabetic() {
                    c2.insert(c);
                }
            }
            // Again, wanted to use `intersection`, but ended up doing an ugly dance around ownership. Couldn't find a way to mutate in place
            let intersection = chars.intersection(&c2).collect::<HashSet<_>>();
            //( ew
            let mut t = HashSet::new();
            for &&i in intersection.iter() {
                t.insert(i);
            }
            chars.clear();
            for &i in t.iter() {
                chars.insert(i);
            }
            //) ew
        }
        chars.len()
    }).sum()
}