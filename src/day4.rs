use itertools;
use itertools::Itertools;
use std::str::FromStr;
use text_io;
use std::error::Error;




// DISCLAIMER: If you want to keep your sanity, don't look at the Dirty version.
// P.S.: there's no other version as of right now...



#[aoc(day4, part1, Dirty)]
fn tree_count(input: &str) -> usize {
    let mut ks: Vec<&str> = Vec::new();
    let mut pwds = 0;
    for l in input.lines() {
        if l.is_empty() {
            println!("empty");
            if ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"/*, "cid"*/].iter().filter(|k| {ks.contains(k)}).count() == 7 {
                if ks.len() == 7 || (ks.len() == 8 && ks.contains(&"cid")) {
                    println!("{:?} ok {:?}", ks, ks.len());
                    pwds += 1;
                }
            }
            println!("      {:?}", ks);
            ks.clear();
        }
        else {for w in l.trim().split(' ').into_iter() {
            ks.push(w.trim().split(':').collect::<Vec<&str>>()[0]);
        }}
    }
    if ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"/*, "cid"*/].iter().filter(|k| {ks.contains(k)}).count() == 7 {
                if ks.len() == 7 || (ks.len() == 8 && ks.contains(&"cid")) {
                    println!("{:?} ok {:?}", ks, ks.len());
                    pwds += 1;
                }
            }

    pwds
}

fn check_parse(k: &str, v: &str) -> Result<(), ()> {
    dbg!((k, v));
    match k {
        "byr" => {if v.len() != 4 {return Err(());} let y = v.parse::<usize>(); if y.is_err() {return Err(());} else if (1920usize..=2002usize).contains(&y.unwrap()) {return Ok(())}; return Err(());},
        "iyr" => {if v.len() != 4 {return Err(());} let y = v.parse::<usize>(); if y.is_err() {return Err(());} else if (2010usize..=2020usize).contains(&y.unwrap()) {return Ok(())}; return Err(());},
        "eyr" => {if v.len() != 4 {return Err(());} let y = v.parse::<usize>(); if y.is_err() {return Err(());} else if (2020usize..=2030usize).contains(&y.unwrap()) {return Ok(())}; return Err(());},
        "hgt" => {
            let mut nums = true;
            let mut digits = String::new();
            let mut alphas = String::new();
            for l in v.bytes() {
                let l = l as char;
                if nums {
                    if dbg!(l).is_digit(10) {
                        println!("digit");
                        digits.push(l);
                    }
                    else {
                        nums = false;
                    }
                }
                if !nums {
                    if dbg!(l).is_alphabetic() {
                        println!("alpha");
                        alphas.push(l);
                    }
                }
            }
            let d = digits.parse::<usize>();
            if d.is_err() {println!("NOT A DIGIT {}", d.err().unwrap()); return Err(())}
            if ["cm", "in"].contains(&alphas.as_str()) {
                if alphas == "cm" {
                    if !(150usize..=193usize).contains(&d.ok().unwrap()) {return Err(());}
                }
                else {
                    if !(59usize..=76usize).contains(&d.ok().unwrap()) {return Err(());}
                }
                Ok(())} else {println!("NOT A CM IN"); Err(())}
            },
        "hcl" => {if v.len() != 7 || (v.as_bytes()[0] as char).to_string() != "#" {return Err(());}
            for i in v.bytes().skip(1) {
                let i = i as char;
                if !["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"].contains(&i.to_string().as_str()) {return Err(())}
        }
        Ok(())},
        "ecl" => {if ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].contains(&v) {Ok(())} else {Err(())}},
        "pid" => {if v.len() == 9 && v.parse::<usize>().is_ok() {Ok(())} else {Err(())}},
        //"cid" => {},
        _ => Ok(())
    }
}



#[aoc(day4, part2, Dirty)]
fn tree_count_fields(input: &str) -> usize {
    //let input: &str = "";
    let mut ks: Vec<&str> = Vec::new();
    let mut vs: Vec<&str> = Vec::new();
    let mut pwds = 0;
    for l in input.lines() {
        if l.is_empty() {
            println!("empty");
            println!("      {:?}", ks);
            println!("      {:?}", vs);
            if ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"/*, "cid"*/].iter().filter(|k| {ks.contains(k)}).count() == 7 {
                println!("::::");
                if ks.iter().enumerate().filter(|(i, k)| dbg!(check_parse(k, vs[*i])).is_ok()).count() == ks.len() {
                    println!("------- ok --------



                    ");
                    pwds += 1;
                }println!("--------



                    ");
            }
            ks.clear();
            vs.clear();
        }
        else {for w in l.trim().split(' ').into_iter() {
            let r = w.trim().split(':').collect::<Vec<&str>>();
            ks.push(r[0]);
            vs.push(r[1]);
        }}
    }
            if ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"/*, "cid"*/].iter().filter(|k| {ks.contains(k)}).count() == 7 {
                println!("::::");
                println!("{:?}", ks);
                println!("{:?}", vs);
                if ks.iter().enumerate().filter(|(i, k)| dbg!(check_parse(k, vs[*i])).is_ok()).count() == ks.len() {
                    println!("------- ok --------



                    ");
                    pwds += 1;
                }
            }


    pwds
}



/*
#[aoc(day3, part2, Dirty)]
fn tree_counts(forest: &str) -> usize {
    let mut i = 0;
    let mut j = 0;
    let mut k = 0;
    let mut m = 0;
    let mut n = 0;
    let mut n_en = false;
    let forest = forest.lines();
    let len = forest.clone().next().unwrap().len();
    let mut c = [0, 0, 0, 0, 0];
    for l in forest.skip(1) {
        i = (i+3)%len;
        j = (j+1)%len;
        k = (k+5)%len;
        m = (m+7)%len;
        if l.as_bytes()[i] as char == '#' {
            c[0] += 1;
        },
        if l.as_bytes()[j] as char == '#' {
            c[1] += 1;
        }
        if l.as_bytes()[k] as char == '#' {
            c[2] += 1;
        }
        if l.as_bytes()[m] as char == '#' {
            c[3] += 1;
        }
        if n_en {
            n = (n + 1) % len;
            if l.as_bytes()[n] as char == '#' {
                c[4] += 1;
            }
        }
        n_en = !n_en;
    }
    c[0] * c[1] * c[2] * c[3] * c[4]
}



#[aoc(day3, part2, Iter)]
fn tree_counts_iter(forest: &str) -> usize {
    let mut is: [usize; 4] = [0, 0, 0, 0];
    let mut j: usize = 0;
    let mut n_en = false;

    let coefs = [3, 1, 5, 7];

    let mut cs = [0, 0, 0, 0];
    let mut cj = 0;

    let forest = forest.lines();
    let len = forest.clone().next().unwrap().len();
    for l in forest.skip(1) {
        let _: Vec<()> = is.iter_mut().zip(coefs.iter()).zip(cs.iter_mut()).map(|((i, coef), c)| {*i = (*i+coef)%len; if l.as_bytes()[*i] as char == '#' {*c += 1};}).collect();
        if n_en {
            j = (j + 1) % len;
            if l.as_bytes()[j] as char == '#' {
                cj += 1;
            }
        }
        n_en = !n_en;
    }
    cs[0] * cs[1] * cs[2] * cs[3] * cj
}

 */