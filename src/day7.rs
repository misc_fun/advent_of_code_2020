use itertools;
use itertools::Itertools;
use std::str::FromStr;
use text_io;
use std::error::Error;
use std::io::Read;
use std::collections::HashSet;
use std::convert::TryInto;

#[derive(Debug, PartialOrd, PartialEq)]
struct Container<'a> {
    name: &'a str,
    children: Vec<(usize, &'a str)>,
}

impl<'a> From<&mut Vec<&'a str>> for Container<'a> {
    fn from(mut s: &mut Vec<&'a str>) -> Self {
        let mut children = Vec::new();
        for b in s.iter_mut().skip(1) {
            let number: usize = b.split(" ").next().unwrap().parse().unwrap_or(0);
            *b = b.trim_start_matches(&(number.to_string() + " "));//could've rsplitted as well, but for the sake of learning to use different tools, `trim_start_matches` is used here
            if number > 0 {
                children.push((number, *b));
            }
        }
        Container {
            name: s[0],
            children
        }
    }
}
impl<'a> PartialEq<&'a str> for Container<'a> {
    fn eq(&self, other: &&'a str) -> bool {
        self.name == *other
    }
}

// how to make generators work with lifetimes?
fn gen<'a>(input: &'a str) -> Vec<Container<'a>> {
    let mut c: Vec<Vec<&'a str>> = input.lines().collect::<Vec<&'a str>>().iter().map(|l| {l.split(" bags contain ").collect()}).collect();
    let mut containers: Vec<Container> = Vec::new();
    for l in c.iter_mut() {
        let list_children = l.remove(1);
        let mut li: Vec<&'a str> = list_children.split(",").collect();
        l.append(&mut li);
        for b in l.iter_mut() {
            // Over trimming, just in case
            *b = b.trim();
            *b = b.trim_end_matches(".");
            *b = b.trim_end_matches("bags");
            *b = b.trim_end_matches("bag");
            *b = b.trim();
        }
        containers.push(l.into());
    }
    containers
}




#[aoc(day7, part1, Dirty)]
pub fn luggage_containing(input: &str) -> usize {
    let mut containers = gen(input);
    let mut children = HashSet::new();

    // inserting the initial shiny gold bag
    children.insert("shiny gold");

    'list: loop {
        'iter: for c in containers.iter_mut() {
            if c.children.iter().filter(|(_, n)| children.contains(n)).count() > 0 {children.insert(c.name); c.children.clear(); continue 'list;}
        }
        break 'list;
    }
    children.len() - 1
}

#[aoc(day7, part2, Dirty)]
pub fn luggage_inside(input: &str) -> usize {
    let mut containers = gen(input);
    let mut total: usize = 0;
    let mut children = Vec::new();
    let mut new_children = Vec::new();

    // inserting the initial shiny gold bag's children
    containers.iter().find(|bag| bag.name == "shiny gold").unwrap().children.iter().map(|b| children.push((b.0, *b))).count();

    //Todo: Cleaner approach: children.iter_mut().map_while(|container| {})

    loop {
        for c in containers.iter_mut() {
            let targets: Vec<(usize, &(usize, (usize, &str)))> = children.iter().enumerate().filter(|(i, b)| {b.1.1 == c.name}).collect();
            let targets = targets.clone();
            for t in targets {
                total += t.1.0;
                c.children.iter().map(|c| {new_children.push((c.0*t.1.0, *c));}).count();// how to consume the iterator in a clean way when the result is unimportant? (collect will ask for the return type even when it's just a vector of Unit elements)
            }
        }
        if new_children.len() == 0 {break;}

        // not really useful (this is the Dirty version, whose aim is to have something that works, and explore different -not necessarily optimal at all- ways to do something; test the limits and play with concepts
        children = new_children.clone();
        new_children.clear();
    }
    total
}
