use itertools;
use itertools::Itertools;
use std::str::FromStr;
use text_io;
use std::error::Error;
use std::io::Read;
use std::collections::HashSet;
use std::convert::TryInto;
use std::ops::Neg;

fn gen(input: &str) -> Vec<(&str, isize)> {
     input.lines().map(|l| {let mut s = l.split(" "); (s.next().unwrap(), s.next().unwrap().parse().unwrap())}).collect()
}


#[aoc(day8, part1, Dirty)]
pub fn to_loop(input: &str) -> isize {
    let input: Vec<(&str, isize)> = gen(input);
    let mut done = vec![false; input.len()];
    let mut i = 0;
    let mut acc = 0;
    loop {
        if done[i] {
            break acc;
        }
        done[i] = true;
        match input[i] {
            ("acc", n) => {acc += n; i += 1;},
            ("jmp", n) => i = (i as isize + n) as usize,
            ("nop", _) => {i += 1;},
            _ => unreachable!()
        }
    }
}
/*
pub fn not_to_loop(input: &str) -> isize {
    let input: Vec<(&str, isize)> = gen(input);
    let mut origin = vec![(vec![], vec![]); input.len()];
    for (i, v) in input.iter().enumerate() {
        match v {
            ("acc", _) => {},
            ("jmp", n) => { origin[(i as isize + n) as usize].0.push(i);},
            ("nop", _) => { origin[(i as isize + n) as usize].1.push(i);},
            _ => unreachable!()
        }
    }
    let mut done = vec![false; input.len()];
    let mut i = 0;
    let mut last_jump = 0;
    loop {
        if done[i] {
            break;
        }
        done[i] = true;
        match input[i] {
            ("acc", n) => {i += 1;},
            ("jmp", n) => {last_jump = i; i = (i as isize + n) as usize},
            ("nop", _) => {i += 1;},
            _ => unreachable!()
        }
    }



    let mut entries = Vec::new();
    let mut new_entries = Vec::new();
    entries.push(input.len()-1);
    while entries.len() != 0 {
        for i in entries {
            let mut j = i;
            while !done[j] {
                if is_jmp_up(input[j]) {

                }
                j -= 1;
            }
        }

        loop {
            match input[i] {
                ("acc", n) => {acc += n; i += 1;},
                ("jmp", n) => i = (i as isize + n) as usize,
                ("nop", _) => {i += 1;},
                _ => unreachable!()
            }
            i -= 1;
        }
    }
    0
}

fn is_jmp_up((op, n): (&str, isize)) -> bool {
    if op == "jmp" && n < 0 {true} else {false}
}

*/

#[aoc(day8, part2, Dirty)]
fn not_to_loop(input: &str) -> isize {
    let input: Vec<(&str, isize)> = gen(input);
    let mut done = vec![false; input.len()];
    return iterate(&input, 0, done, 0, false).unwrap();
}
fn iterate(input: &Vec<(&str, isize)>, mut i: usize, mut done: Vec<bool>, mut acc: isize, changed: bool) -> Option<isize> {
    let d = input.len();
    loop {
        if done[i] {
            break None;
        }
        else {
            done[i] = true;
        }
        match input[i] {
            ("acc", n) => { acc += n; i += 1; },
            ("jmp", n) => {
                if !changed {if let Some(a) = iterate(&input, i + 1, done.clone(), acc, true) {
                    return Some(a);
                }}
                i = (i as isize + n) as usize
            },
            ("nop", n) => {
                if !changed {if let Some(a) = iterate(&input, (i as isize + n) as usize, done.clone(), acc, true) {
                    return Some(a);
                }}
                i += 1;
            },
            _ => unreachable!()
        }
        if i == d {return Some(acc);}
    }
}