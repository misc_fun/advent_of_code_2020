extern crate aoc_runner;

#[macro_use]
extern crate aoc_runner_derive;


pub mod day1;
pub mod day2;
pub mod day3;
pub mod day4;
pub mod day5;
mod day6;
mod day7;
mod day8;
mod day9;
mod day10;


aoc_lib!{ year = 2020 }


#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
