use itertools;
use itertools::Itertools;
use std::str::FromStr;
use text_io;

#[aoc(day3, part1, Dirty)]
fn tree_count(forest: &str) -> usize {
    let mut i = 0;
    let forest = forest.lines();
    let len = forest.clone().next().unwrap().len();
    let mut c = 0;
    for l in forest.skip(1) {
        i = (i+3)%len;
        if l.as_bytes()[i] as char == '#' {
            c += 1;
        }
    }
    c
}

#[aoc(day3, part2, Dirty)]
fn tree_counts(forest: &str) -> usize {
    let mut i = 0;
    let mut j = 0;
    let mut k = 0;
    let mut m = 0;
    let mut n = 0;
    let mut n_en = false;
    let forest = forest.lines();
    let len = forest.clone().next().unwrap().len();
    let mut c = [0, 0, 0, 0, 0];
    for l in forest.skip(1) {
        i = (i+3)%len;
        j = (j+1)%len;
        k = (k+5)%len;
        m = (m+7)%len;
        if l.as_bytes()[i] as char == '#' {
            c[0] += 1;
        }
        if l.as_bytes()[j] as char == '#' {
            c[1] += 1;
        }
        if l.as_bytes()[k] as char == '#' {
            c[2] += 1;
        }
        if l.as_bytes()[m] as char == '#' {
            c[3] += 1;
        }
        if n_en {
            n = (n + 1) % len;
            if l.as_bytes()[n] as char == '#' {
                c[4] += 1;
            }
        }
        n_en = !n_en;
    }
    c[0] * c[1] * c[2] * c[3] * c[4]
}



#[aoc(day3, part2, Iter)]
fn tree_counts_iter(forest: &str) -> usize {
    let mut is: [usize; 4] = [0, 0, 0, 0];
    let mut j: usize = 0;
    let mut n_en = false;

    let coefs = [3, 1, 5, 7];

    let mut cs = [0, 0, 0, 0];
    let mut cj = 0;

    let forest = forest.lines();
    let len = forest.clone().next().unwrap().len();
    for l in forest.skip(1) {
        let _: Vec<()> = is.iter_mut().zip(coefs.iter()).zip(cs.iter_mut()).map(|((i, coef), c)| {*i = (*i+coef)%len; if l.as_bytes()[*i] as char == '#' {*c += 1};}).collect();
        if n_en {
            j = (j + 1) % len;
            if l.as_bytes()[j] as char == '#' {
                cj += 1;
            }
        }
        n_en = !n_en;
    }
    cs[0] * cs[1] * cs[2] * cs[3] * cj
}