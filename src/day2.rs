use itertools;
use itertools::Itertools;
use std::str::FromStr;
use text_io;

#[derive(Debug, Clone)]
struct Pwd {
    min: usize,
    max: usize,
    lt: char,
    pwd: String,
}

impl FromStr for Pwd {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let min;
        let max;
        let lt;
        let pwd;
        text_io::scan!(s.bytes() => "{}-{} {}: {}", min, max, lt, pwd);
        Ok(Pwd {
            min,
            lt,
            max,
            pwd
        })
    }
}
impl Pwd {
    fn check(&self, i: usize) -> bool {
        if let Some(ci) = self.pwd.as_bytes().get(i) {
            if self.lt == *ci as char {
                return true
            }
        }
        false
    }
}

#[aoc_generator(day2)]
fn gen(input: &str) -> Vec<Pwd> {
    input.lines().filter_map(|s| Some(s.trim().parse().unwrap())).collect()
}



#[aoc(day2, part1, Dirty)]
fn password_check(passwords: &[Pwd]) -> usize {
    let mut ps = 0;
    for p in passwords {
        let n: usize = p.pwd.chars().fold(0, |a, l| {if l == p.lt {a + 1} else {a}});

        if  (p.min..=p.max).contains(&n) {
            ps += 1;
        }
    }
    ps
}


#[aoc(day2, part2, Dirty)]
fn password_check_2(passwords: &[Pwd]) -> usize {
    passwords.iter().filter(|p| p.check(p.min-1) != p.check(p.max-1)).count()
}